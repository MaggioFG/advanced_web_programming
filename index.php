<?php
require_once ('webdb_connection.php');
require_once('functions.php');
require_once('db_functions.php');
session_start();

if (isset($_POST['logout'])){
	  $_SESSION['logout_success'] = 'Logged out successfully';
	  header("location: logout.php");
	  return;
}
if (isset($_POST['go_index'])){
	header("location: index.php");
	return;
}
if (isset($_SESSION['username'])){
	setcookie('username', $_SESSION['username'], time() + 86400);
	if (strcasecmp($_SESSION['username'], 'Admin') == 0){
		$_SESSION['admin'] = 'admin'; 
	}	
}

$category_check['electronics'] = isset($_GET['category_electronics']) ? 'checked' : '';
$category_check['food'] = isset($_GET['category_food']) ? 'checked' : '';
$category_check['clothes'] = isset($_GET['category_clothes']) ? 'checked' : '';

$search_string = '';
if (isset($_GET['search'])){
		$search_string = $_GET['search_string'];
}

$where_clause = '';

if (!is_empty_array_values($category_check)){
		
		foreach ($category_check as $key => $value) {
			/* 
			 only the checked category has to appear in the where clause
			 the array is $category_check['name_of_category'] => value
			 where value is '' if the category is not checked, 'checked' otherwise 
			 at first constraint found where clause is empty and must be created 
			 */
			if(!is_empty_array_values($value) && !$where_clause){
					$where_clause = ' WHERE category="' . $key . '" ';
			}
			// After the first category constraint the others ar added in OR
			elseif (!is_empty_array_values($value)){
				  $where_clause = $where_clause . ' OR category="' . $key .'" ';
			}
		}    
}
if($search_string){
		if (!$where_clause){
			$where_clause = ' WHERE name LIKE "%' . $search_string . '%" '; 
		}else{
			$where_clause = $where_clause  
										  . ' AND name LIKE "%' . $search_string . '%" ';
		}
}

if($where_clause){
	  $results = $web_db->query(' SELECT * FROM product ' . $where_clause );
}else{
	  $results = $web_db->query('SELECT * FROM product ');
}
    


?>

<!DOCTYPE html>
<html>
<head>
  <script src="js/js_cookie_functions.js"></script>
  <link rel="stylesheet" href="css/bulma.min.css">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  

  <title>Home page</title>
</head>
<body style="font-family:sans-serif">
    
  <?php
      if(isset($_SESSION['username'])){
          require_once('navbar_logged.html');
      }
      else{
          require_once('navbar_not_logged.html');
      }
  ?>
  <?php	
	if (isset($_SESSION['login_success'])){
		echo ("<p style='color:green'> " . $_SESSION['login_success'] . "<p> \n");
		unset($_SESSION['login_success']);
	}
	if (isset($_SESSION['username'])){
		/* echo("<p> You are correctly logged in as : " . 
			 $_SESSION['username'] . " </p> \n"); */
		if (isset($_SESSION['admin'])){
			echo ("<p> You can add or edit the products catalogue 
				   <a href='add_product.php'> here </a>");
		}
		/* echo("<p> You can log out here 
						<form	method='POST'>
							<input type='submit' class='button' name='logout' value='logout'/>
						</form>
					</p>"); */
	} /* else {
		echo("<p> Please <a href='login.php'> login </a> to start </p> \n");
		echo("<p> If you don't have an account you can register 
					<a hris-paddinglessef='signup.php'> here </a>");
	}
	*/
	if (isset($_SESSION['forbidden'])){
        echo ("<div class='notification is-danger'> ACCESS FORBIDDEN </div>");
	//	echo ("<p style='color:red'> " . $_SESSION['forbidden'] . "</p>");
		unset($_SESSION['forbidden']);
    }
	
  ?>
 
  <section class="section">
    <div class="columns">
      <div class="column is-one-fifth has-background-light">
	    
	    <b>category</b>
	    <form method="GET" id="filter">
		  <ul>
		    <li>
		      <input type="checkbox" name="category_electronics" 
		  			value="electronics"  id="electronics"
		  			onchange="document.getElementById('filter').submit();"
		  			<?php echo (htmlentities($category_check['electronics']));?>
		  			/>
		      <label for="electronics"> Electronics </label>
		    </li>
		    <li>
		      <input type="checkbox" name="category_food"
		  			 value="food"  id="food"
		  			 onchange="document.getElementById('filter').submit();"
		  			 <?php echo (htmlentities($category_check['food'])); ?>
		  			 />
		      <label for="food"> Food </label>
		    </li>
		    <li>
		      <input type="checkbox" name="category_clothes"
		  		   value="clothes"  id="clothes"
		  		   onchange="document.getElementById('filter').submit();"
		  		   <?php echo (htmlentities($category_check['clothes']));?>
		  		   />
		      <label for="clothes"> Clothes </label>
		    </li>
		  </ul>
	    
      </div>
      <div class="column is-three-fifths">	
		    <div class="field has-addons">
          <div class="control is-expanded">
		        <input class="input" type="text" name="search_string" placeholder="Search"/>
          </div>
          <div class="control">
		  			<input class="button is-info" type="submit" 
		  			       name='search' value='Search'/>
           </div>
		  	</div>
			</form>
			<section class="section">
        <div class="tile is-ancestor is-vertical">
					<div class="tile is-parent is-vertical is-4">
					<?php
						$i = 0;
						foreach ($results as  $row) {
							if($i % 2 == 0){
							    echo('<div class="tile is-parent is-vertical" >');
							    echo('<a href="product_page.php?id=' . htmlentities(
							    	$row['product_id']) . '" >');
							    show_product($row['product_id']);
							    echo ('</a>');
							    
							    echo ('</div>');
							}else{
									echo('<a href="product_page.php?id=' . htmlentities(
										$row['product_id']) . '" >');
									echo('<section class="section">');
										show_product($row['product_id']);
									echo('</section>');
									echo('</a>');
							}
							$i++;
					}
					?>
			  </div>
			</section>
  </section>
</body>
</html>