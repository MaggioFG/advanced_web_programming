<?php
require_once('webdb_connection.php');
require_once('functions.php');
require_once('db_functions.php');
session_start();


if ( !isset($_GET['id']) && !isset($_POST['id'])){
    header('location: index.php');
    return;
}else{
    $product_id = isset($_GET['id']) ? $_GET['id'] : $_POST['id'];
    $select_product = 'SELECT * FROM product WHERE product_id=%s'; 
    $product_info = $web_db->queryFirstRow($select_product, $product_id);
}

if (isset($_POST['add_to_cart'])){
    $i = 0;
    if(isset($_COOKIE['cart'])){
       $i = count(array_filter($_COOKIE['cart']));
    }
    $_SESSION['add_to_cart_success'] = true;
    setcookie("cart[" . $i . "]", $_POST['id'], time() + 86400);
    header('location: product_page.php?id=' . htmlentities($product_id));
    return;
}

?>



<!DOCTYPE html>
<html>
<head>
  <script src="js/js_cookie_functions.js"></script>
  <script src="js/utilities_functions.js"></script>
  <link rel="stylesheet" href="css/bulma.min.css">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>
      <?php print(htmlentities($product_info['name'])); ?>
  </title>
<body style="font-family:sans-serif">
<?php
    if(isset($_SESSION['username'])){
        require_once('navbar_logged.html');
    }
    else{
        require_once('navbar_not_logged.html');
    }
?>  

<?php 
    if (isset($_SESSION['add_to_cart_success'])){
        require_once('message_add_to_cart_success.html');
		unset($_SESSION['add_to_cart_success']);
	}

?>

<?php 
    show_product($product_id);
?>
<form method="POST">
  <input type="text" name='id' 
         value=<?php print("'" . htmlentities($product_info['product_id']) . "'") ?> 
         hidden/>
  <input type="submit" class="button is-primary" 
         name="add_to_cart" value="Add to cart"/>

</form>

</body>
</html>
