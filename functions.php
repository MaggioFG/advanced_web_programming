<?php

function password_error($pwd, $pwd1, &$errors){
    if (!($pwd == $pwd1)){
        $errors[] = "The two passwords don't match";
    }else{
        if (strlen($pwd) < 8){
            $errors[] = "Password too short, use at least 8 character";
        }
        if (!preg_match("#[0-9]+#", $pwd)){
            $errors[] = " Password must contain at least one number";
        }
        if (!preg_match("#[a-z]+#", $pwd)){
            $errors[] = " Password must contain at least one lower case letter";
        }
        if (!preg_match("#[A-Z]+#", $pwd)){
            $errors[] = "Password must contain at least one upper case letter";
        }
    }

}

function is_empty_array_values($array){
    if (is_array($array)) {
        foreach ($array as $value) {
            if (is_array($value)) {
                if (!is_empty_array_values($value)){
                    return false;
                }
            }elseif (!empty($value)){
                return false;
            }
        }
    }
    elseif (!empty($array)){
        return false;
    }

    return true;
}

?>