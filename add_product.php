<?php 
require_once('webdb_connection.php');
require_once('functions.php');
require_once('db_functions.php');
session_start();

if(!isset($_SESSION['username']) || !isset($_SESSION['admin'])){
    $_SESSION['forbidden'] = 'This page is for administrator only';
    header('location:index.php');
    return;
}elseif(isset($_POST['add_product'])){
        if (empty($_POST['name']) || empty($_POST['category'])
            || empty($_POST['price']) || empty($_POST['available_quantity'])){                
                $_SESSION['add_product_error'][] = 'Missing data';
                header('location:add_product.php');
                return;
                  
        }else{
            if(exists_db('name', $_POST['name'], 'product')){
                $_SESSION['add_product_warning'] = 'Product already exists
                                                  in the catalogue,
                                                  product modified';

                $web_db->update('product', array(
                    'category' => $_POST['category'],
                    'price' => $_POST['price'],
                    'available_quantity' => $_POST['available_quantity'])
                    , 'name=%s', $_POST['name']);
                
                
                header('location:add_product.php');
                return;
            }else{
                
                $web_db->insert('product', array(
                    'name' => $_POST['name'],
                    'category' => $_POST['category'],
                    'price' => $_POST['price'],
                    'available_quantity' => $_POST['available_quantity']
                ));
                /* 
                $sql = "INSERT INTO product(name, category, price)
                        VALUES (:name, :category, :price)";
    
                $stmt = $web_db->prepare($sql);
                $stmt->execute(array(
                      ':name' => $_POST['name'],
                      ':category' => $_POST['category'],
                      ':price' => $_POST['price']
                        ));
                 */
                $_SESSION['add_product_success'] = 'Item added';
                header('location:add_product.php');
                return;
            }
        }
}

?>



<!DOCTYPE html>
<html>
<head>
  <script src="js/js_cookie_functions.js"></script>
  <link rel="stylesheet" href="css/bulma.min.css">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>Add new product</title>
</head>

<body style="font-family: sans-serif;">  
  <?php
      if(isset($_SESSION['username'])){
          require_once('navbar_logged.html');
      }
      else{
          require_once('navbar_not_logged.html');
      }
  ?>
  <section class="section has-background-light">
  <?php
      if (isset($_SESSION['add_product_success'])){
          echo ("<p style='color:green'> 
                Product added successfully to the catalogue </p>");
          unset($_SESSION['add_product_success']);
  
      }elseif (isset($_SESSION['add_product_error'])){
          echo ("<p style='color:red'>");  
          echo (htmlentities($_SESSION['add_product_error']));
          echo ("<br> </p>");
          unset($_SESSION['add_product_error']);
      }
      elseif (isset($_SESSION['add_product_warning'])){
          echo ("<p style='color:yellow'>");  
          echo (htmlentities($_SESSION['add_product_warning']));
          echo ("<br> </p>");
          unset($_SESSION['add_product_warning']);
      }
  
  ?>
  
  <h1 class="title">Add a Product</h1>
    <form method='POST' id='add_product_form'>
      <p>
        <label for='name'> Name </label>
        <input type='text' name='name' id='name' class='form_input'
               placeholder="Insert the name of the product" required/>
        <label for='category'> Category </label>
        <select name='category' form='add_product_form' required>
            <option value="electronics"> Electronics </option>
            <option value="food"> Food </option>
            <option value="clothes"> Clothes </option>        
      </p>
      <p>
        <label for='price'> Price </label>
        <input type='number' name='price' placeholder='Price of the product' required/>
      </p>
      <p>
        <label for='available_quantity'> Available quantity </label>
        <input type='number' name='available_quantity' placeholder='available_Quantity of the product' required/>
      </p>
      <!-- <form action="image_upload.php">
        <p>
          <label for='product_image'> Product image </label>
          <input type='file' name='product_image' style="color:green"
                 placeholder='Images of the product' accept="image/*" multiple/>
          <input type="submit" class="button" name="add_product_image" 
                 value="Add images" >
        </p>
      </form> -->
      <input type="submit" class="button" name='add_product' value='Add Product' />
      
    </form>
    <form method="POST" action='index.php'>
        <input type="submit" class="button" name='logout' value='Logout'/> 
    </form>
  </section>
</body>

</html>