function hide_element(id){
    document.getElementById(id).style.display ='none';
}

function validate_password(pwd, pwd1 ){
    if (!(pwd === pwd1)){
        return false;
    }else{
        if (strlen(pwd) < 8){
            return false;
        }
        if (!match("#[0-9]+#", pwd)){
            return false;
        }
        if (!match("#[a-z]+#", pwd)){
            return false;
        }
        if (!match("#[A-Z]+#", pwd)){
            return false;
        }
    }

}