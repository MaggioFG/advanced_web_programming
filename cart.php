<?php
require_once('webdb_connection.php');
require_once('functions.php');
require_once('db_functions.php');
session_start();


$cart_product[] = '';
if (isset($_COOKIE['cart'])){
    foreach ($_COOKIE['cart'] as $key => $value) {
        $product_id[] = $value;
    }
    $quantity = array_count_values($product_id);
    if (!is_empty_array_values($product_id)){
        $select_cart_product = 'SELECT * FROM product WHERE product_id IN %li'; 
        $cart_product = $web_db->query($select_cart_product, array_keys($quantity));
    }
}

//calculate total price and other information for the payment
if (!is_empty_array_values($cart_product)){
  $total = 0;
  foreach ($cart_product as $key => $value) {
    $total += $value['price'] * $quantity[$value['product_id']];  
  }
  $split = $total/2;
  $total = '' . $total;
  $split = '' . $split;
  $payment_details = array(
    'transaction' => array(
      array(
        'amount' => array(
          'total' => '300.00',
          'currency' => 'EUR',
          'details' => array(
            'subtotal' => '300.00',
            'tax' => '0.0'
            )
          ),
          'description' => 'Payment description',
          'custom' => 'custom message',
          'payment_options' => array(
            'allowed_payment_method' => 'INSTANT_FUNDING_SOURCE'
          ),
          'soft_descriptor' => 'ECHI5786786',
          'item_list' => array(
            'items' => array(
              array(
                'name' =>'hat' ,
                'quantity' => '1',
                'price' => '150.00',
                'currency' => 'EUR'
              ),
              array(
                'name' =>'t-shirt' ,
                'quantity' => '1',
                'price' => '150.00',
                'currency' => 'EUR'
              )
            )
          ),
          'shipping_address' => array(
            'recipient_name' => 'Mario Parisi',
            'line1' => 'street 123',
            'city' => 'Timisoara',
            'country_code' => 'RO',
            'postal_code' => '71100'
            )
          )
        ),
        'note_to_payer' => 'Note for the client'
      );
    $payment_json = json_encode($payment_details);
}
          
?>


<!DOCTYPE html>
<html>
<head>
  <script src="js/js_cookie_functions.js"></script>
  <script src="js/utilities_functions.js"></script>
  <link rel="stylesheet" href="css/bulma.min.css">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>
      <?php print(htmlentities($product_info['name'])); ?>
  </title>
<body style="font-family:sans-serif">
<?php
    if(isset($_SESSION['username'])){
        require_once('navbar_logged.html');
    }
    else{
        require_once('navbar_not_logged.html');
    }
?>  

<?php 
    if (isset($product_id)){
        //print('<h1 class="title is-success"> to_cart added </h1>');
        foreach ($cart_product as $value) {
            echo('<section class="section"> ');
            show_product($value['product_id']);
            
            echo('Quantity : ' 
                 . htmlentities($quantity[$value['product_id']]) 
                 . '</section');
        }
	}
?>



<div id="paypal-button"></div>
<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script>
  paypal.Button.render({
    // Configure environment
    env: 'sandbox',
    client: {
      sandbox: 'AQecUXZlcok5YmQ66VyhMYNU4HRDtTGzYhXt1m9IqdXbqtVZagkqxVVZWS83XQXqI56DXFGQpDH9plZ8',
      production: 'demo_production_client_id'
    },
    // Customize button (optional)
    locale: 'en_US',
    style: {
      size: 'medium',
      color: 'gold',
      shape: 'pill',
    },

    // Enable Pay Now checkout flow (optional)
    commit: true,
        //<?php echo($payment_json); ?>

    // Set up a payment
    // Set up a payment
payment: function(data, actions) {
  return actions.payment.create({
    transactions: [{
      amount: {
        total: <?php echo($total) ?>,
        currency: 'EUR',
        details: {
          subtotal: <?php echo($total) ?>,
          tax: '0.0',
          }
      },
      description: 'The payment transaction description.',
      custom: 'custom message',
      //invoice_number: '12345', Insert a unique invoice number
      payment_options: {
        allowed_payment_method: 'INSTANT_FUNDING_SOURCE'
      },
      soft_descriptor: 'ECHI5786786',
      item_list: {
        items: [
        {
          name: <?php echo($total) ?>,
          quantity: '1',//<?php //echo($total) ?>,
          price: <?php echo($split) ?>,
          currency: 'EUR'
        },
        {
          name: <?php echo($total) ?>,
          quantity: '1',//<?php // echo($) ?>,
          price: <?php echo($split) ?>,
          currency: 'EUR'
        }],
        shipping_address: {
          recipient_name: 'Mario Parisi',
          line1: 'street 123',
          city: 'Timisoara',
          country_code: 'RO',
          postal_code: '71100'
        }
      }
    }],
    note_to_payer: 'Note for the client'
  });
},
    // Execute the payment
    onAuthorize: function(data, actions) {
      return actions.payment.execute().then(function() {
        // Show a confirmation message to the buyer
        //#TODO eliminate cart cookies
        window.alert('Thank you for your purchase!');
      });
    }
  }, '#paypal-button');

</script>


<?php  echo($payment_json); ?>

</body>
</html>
