<?php
require_once('webdb_connection.php');
require_once('functions.php');
require_once('db_functions.php');
session_start();


if (isset($_POST['field'])){
    $_SESSION['field_success'] = 'message';
    $_COOKIE['cart'] = $_POST['id'];
    header('location: index.php');
    return;
}

?>


<!DOCTYPE html>
<html>
<head>
  <script src="js/js_cookie_functions.js"></script>
  <script src="js/utilities_functions.js"></script>
  <link rel="stylesheet" href="css/bulma.min.css">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>
      <?php print(htmlentities($product_info['name'])); ?>
  </title>
<body style="font-family:sans-serif">
<?php
    if(isset($_SESSION['username'])){
        require_once('navbar_logged.html');
    }
    else{
        require_once('navbar_not_logged.html');
    }
?>  

<?php 
    if (isset($_SESSION['message'])){
        //print('<h1 class="title is-success"> to_cart added </h1>');
        require_once('message_message.html');

		unset($_SESSION['message']);
	}

?>

</form>

</body>
</html>
