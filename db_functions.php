<?php
require_once ('webdb_connection.php');

function exists_db($field, $to_search, $table_name){
    global $web_db;
    $sql = "SELECT * FROM " . $table_name . " WHERE " . $field . "= %s";
    $row = $web_db->queryFirstRow($sql, $to_search);

    if (empty($row)){
        return false; 
    }
    else{
        return true;
    }
}

function username_error($username, &$errors){
    global $web_db;
    if (exists_db('username', $username, 'user')){
        $errors[] = "Username already exists"; 
    }
}

function email_error($email, &$errors){
    global $web_db;
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
        $errors[] = "Invalid email";
    }
    if(exists_db('email', $email, 'user')){
        $errors[] = "email already exists";
    }
}

function show_product($product_id){
    global $web_db;
    $row = $web_db->queryFirstRow('SELECT * FROM product WHERE product_id=%i', $product_id);
    
    echo('<div class="tile is-child  box">
            <p class="title">'
               . htmlentities($row["name"]) 
               . '</p>
            <p class="subtitle">'
               . htmlentities($row["category"]) 
               . '</p>
            <p class="subtitle is-pulled-right has-text-info">'
               . htmlentities($row["price"]) 
               . '&euro;</p>
            <p class="subtitle"> Availability: '
               . htmlentities($row["available_quantity"]) 
               . '</p>
		</div>'
    );

}

?>