<?php
session_start();
require_once ('webdb_connection.php');
if (isset($_POST['login'])){
	if (empty($_POST['username']) || empty($_POST['password'])){
		$_SESSION['error'] = 'Missing data';
	    header('location:login.php');
		return;
	}else{
	    	
	    unset($_SESSION['username']);	//Logs out the current user,if any
	
		$sql = "SELECT username,password FROM user 
	    		WHERE username = %s";
		$row = $web_db->queryFirstRow($sql, $_POST['username']);
		/* $stmt = $web_db->prepare($sql);
		   $stmt->execute(array(
		   		':username' => $_POST['username']));
		
		$row = $stmt->fetch((PDO::FETCH_ASSOC)); */
		if(empty($row)){
			$_SESSION['error'] = "User " . htmlentities($_POST['username'])
								 . " doesn't exists";
			header('location:login.php');
			return;
		}elseif(password_verify($_POST['password'], $row['password'])){
			$_SESSION['username'] = $_POST['username'];
			setcookie('username', $_SESSION['username'],time() + 86400);
	    	$_SESSION['login_success'] = htmlentities('Logged in');
	    	header("location:index.php");
	    	return;
	    }else{
			$_SESSION['error'] = 'Incorrect Password for the user : ' 
								. htmlentities($_POST['username']) . ' ';
	    	header('location:login.php');
	    	return;
	    }
	}	
}

?>


<!DOCTYPE html>
<html>
<head>
  <script src="js/js_cookie_functions.js"></script>
  <link rel="stylesheet" href="css/bulma.min.css">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>Login page</title>
</head>

<body style="font-family: sans-serif;">

<?php
	if(isset($_SESSION['username'])){
		require_once('navbar_logged.html');
	}
	else{
		require_once('navbar_not_logged.html');
	}
	if (isset($_SESSION['error'])){
		echo("<p style='color:red;'> " . htmlentities($_SESSION['error']) . "<p> \n");
		unset($_SESSION['error']);
	}
	if (isset($_SESSION['signup_success'])){
		echo ("<p style='color:green'> Registration succeded </p>");
		unset($_SESSION['signup_success']);
	}

?>

  <h1 class="title">Login</h1>
  <p>
  	<form method="POST">
  		<p>
  		  <label for="username"> Username : </label>
  		  <input type="text" name="username" id="username" required/>
  		</p>
  		<p>
  		  <label for="password"> Password : </label>
  		  <input type="password" name="password" id="password" required/>
  		</p>
  		  <input type="submit" class="button" name="login" value="login">
	</form>  	
	<form method='POST' action='signup.php' style='display: inline;'>
		<input type="submit" class="button" name='create_account' value='Create new account'/>
	</form>  
  </p>
  <p> 
<!--  
	  <form action="index.php" method="POST">
  	  <input type="submit" class="button" name="logout"  value="Logout">
	  </form>
-->
</body>

</html>